terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}


variable "ami" {
    type = string
}

variable "instance" {
    type = string
}

variable "server_port" {
    type = number
}

provider "aws"{
    region = "us-east-2"
    access_key = "kiss me"
    secret_key = "then I give you the key"
}

resource "aws_instance" "vm1"{
    #ami = "ami-0c55b159cbfafe1f0"
    ami = var.ami
    #instance_type = "t2.micro"
    instance_type = var.instance
    vpc_security_group_ids = [aws_security_group.bodrovsec.id]
    tags = {
        Name = "vm_1"
    }
}

resource "aws_instance" "vm2"{
    #ami = "ami-0c55b159cbfafe1f0"
    ami = var.ami
    #instance_type = "t2.micro"
    instance_type = var.instance
    vpc_security_group_ids = [aws_security_group.bodrovsec.id]
    tags = {
        Name = "vm_2"
    }
}



resource "aws_security_group" "bodrovsec" {
    name = "terraform-example-instance"
    ingress {
        from_port = var.server_port
        to_port = var.server_port
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
